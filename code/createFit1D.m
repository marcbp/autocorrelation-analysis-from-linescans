function [fitresult, gof] = createFit1D(t, c,std)

%presettings
prompt = {'Enter gamma:','Enter n:','Enter D:','Enter w_{xy}:','Enter offset','Enter selector:'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0.35','50','0.15','0.33','0.001','2'};
answer = inputdlg(prompt,dlgtitle,dims,definput)
d=str2double(answer)

gamma=d(1);
n=d(2);
D=d(3);
wxy=d(4);
offset=d(5);
fix=d(6);

%create weights out of standard deviation by weighing the ones with a low
%standard deviation more than the other way around;replace the Inf values
%with 0 to give the first values low importance
weight=1./std;
index=find(weight==Inf);
weight(index)=0;

if fix==0
    gammastart=0.35;
    gammalow=0;
    gammahigh=+Inf;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0.000001;
    offsetlow=-5;
    offsethigh=5;
elseif fix==1
        gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0;
    offsetlow=-5;
    offsethigh=5;
    elseif fix==2
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=wxy;
    wxylow=wxy;
    wxyhigh=wxy;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    elseif fix==3
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=wxy;
    wxylow=wxy;
    wxyhigh=wxy;
    offsetstart=offset;
    offsetlow=offset;
    offsethigh=offset;
    elseif fix==4
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=D;
    Dlow=D;
    Dhigh=D;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    elseif fix==5
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=n;
    nlow=n;
    nhigh=n;
    Dstart=D;
    Dlow=D;
    Dhigh=D;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
end

%% Fit
%only prepares the vectors to be column vectors
%here transpose the vectors only
[xData, yData] = prepareCurveData( t, c );

% Set up fittype and options.
ft = fittype( '(gamma/n)*(1./sqrt(1+4*D*x/(wxy^2)))+offset', 'independent', 'x', 'dependent', 'y','coefficients', {'gamma','n','D','wxy','offset'} );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [gammastart nstart Dstart wxystart offsetstart];
opts.Lower = [gammalow nlow Dlow wxylow offsetlow];
opts.Upper = [gammahigh nhigh Dhigh wxyhigh offsethigh];
opts.Weights=weight;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
end

