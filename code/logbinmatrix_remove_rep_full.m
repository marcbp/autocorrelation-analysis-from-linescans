function [tlogbin,clogbin,clogstd]=logbinmatrix_remove_rep_full(t,g,bins)

[m, ~]=size(g);

clogbin=zeros([bins m]);
clogstd=zeros([bins m]);

for i=1:1:m
   
   [tlogbin,a,b]=logbinning(t,g(i,:),bins); 
   clogbin(:,i)=a;
   clogstd(:,i)=b;
   
end
%elimination of replicates at the beginning - here not just looks at first
%column, but at all and finds minimum amount of values to remove
%places column indices of where the condition is met in col 
[~,col,~]=find(tlogbin==tlogbin(1,:) & clogbin==clogbin(1,:));
%stores in C the unique values of col in order and only once and in ic each 
%value as it appears in the original matrix
[C,~,ic]=unique(col);
%accumarray uses an input here ic which defines the groups (and would put 
%it into the ascending order, but here already done) and takes each value 
%only once then it would use the values (where there is a 1 now) if 
%multiple of the same index in ic and add them. Since there is a one it 
%adds ones as many as belong to the same position/index in ic. This is how 
%accumarray counts data
%then this is combined with unique 
a_counts = accumarray(ic,1);

%removes the values below the minimum duplicate value
tlogbin(C<min(a_counts))=[];
clogbin(C<min(a_counts),:)=[];
clogstd(C<min(a_counts),:)=[];
end