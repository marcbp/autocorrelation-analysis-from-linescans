function [jac,tr]=assign_correct_labels(tlogbin, clogbin)


%calculate the decimal vector of the time scale for imagesc
tr=10.^(ceil(log10(min(tlogbin))):1:floor(log10(max(tlogbin))));
if floor(max(tlogbin)/10^floor(log10(max(tlogbin))))>floor(log10(max(tlogbin)))
    %in case there is one more after the last decade insert it additionally
tr=[tr, max(tr)*floor(max(tlogbin)/10^floor(log10(max(tlogbin))))];
end

%get the indices of the value closest to the decades
for j=1:length(tr)
[kac(j),jac(j)]=min(abs(tlogbin-tr(j)));
end


%calculate the image and assign the ticks and the labels (where this is
%just a rounded value to make it look nicer (especially the last value was
%rounded from 28.18 to 30)
%imagesc(clogbin_help);
%yticks(jac);
%yticklabels(10.^round(log10(t(j))))
%yticklabels(tr);

end
