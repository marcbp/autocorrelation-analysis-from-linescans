%in case there are more than 1 image chunk
if size(ACs,2)>1 
    %in this part all the lines from each image chunk (tubule) get
    %concatenated. This way there is a multidimensional array as if all the chunks were just 1 image. It's the same as with 1 image chunk. This is the final form in which the matrix is also
    %given to ac_selectorp when there is only 1 image chunk. Then it can be
    %resumed where left off.
chelp=cell2mat(ACs(1));
chelp2=[];
for i=2:size(ACs,2)
    c2=cell2mat(ACs(i));
    chelp=cat(3,chelp,c2);
end
else
chelp=Clogbin;   
end

%do the line autocorrelation selection
    [Clogbinm,CStd,Clog_orig,Clog_final] = ac_selectorp_axis(tlogbin, chelp);
    
%plot the correlation curve
fav=figure( 'Name', 'averaged and logged' );
hav=plot( tlogbin, Clogbinm, 'b--.' );
hold on; 
errorbar(tlogbin, Clogbinm, CStd,'b','LineStyle','none');
legend( hav, 'data', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on

%plot with fit
[fitresultav, gofav] = createFit1D(tlogbin, Clogbinm,CStd)
rav=coeffvalues(fitresultav)

% ignore 0 standard deviation values, just for plotting by including NaN
% values
% index=find(clogbin_stdchunk(:,i)==0);
% clogbin_stdchunk(index,i)=NaN;

% Plot fit with data.
ffit=figure( 'Name', 'Fitted autocorrelation' );
hfit = plot( fitresultav, tlogbin, Clogbinm, 'b--.' );
hold on; 
errorbar(tlogbin, Clogbinm, CStd,'b','LineStyle','none');
legend( hfit, 'data', 'fit', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on


