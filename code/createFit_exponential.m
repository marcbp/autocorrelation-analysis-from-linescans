function [fitresult, gof] = createFit_exponential(t, c,std)

%presettings
prompt = {'Enter n:','Enter offset','Enter selector:','Enter fraction of triplet state:','Enter triplet state constant:'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'50','0.001','2','.2','0.1'};
answer = inputdlg(prompt,dlgtitle,dims,definput)
d=str2double(answer)

n=d(1);
offset=d(2);
fix=d(3);
T=d(4);
tauT=d(5);

%create weights out of standard deviation by weighing the ones with a low
%standard deviation more than the other way around;replace the Inf values
%with 0 to give the first values low importance
weight=1./std;
index=find(weight==Inf);
weight(index)=0;

if fix==0
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    offsetstart=0.000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
elseif fix==1
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    offsetstart=0;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==2
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==3
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    offsetstart=offset;
    offsetlow=offset;
    offsethigh=offset;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==4
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==5
    nstart=n;
    nlow=n;
    nhigh=n;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
end

%% Fit: 
%only prepares the vectors to be column vectors
%here transpose the vectors only
[xData, yData] = prepareCurveData( t, c );

% Set up fittype and options.
%fit of bleaching T=B
ft = fittype( '1/n*((1-T+T*exp(-x/tauT)))+offset', 'independent', 'x', 'dependent', 'y','coefficients', {'n','offset','T','tauT'} );
%Triplet state term without diffusion fitting function
%ft = fittype( '1/n*((1-T+T*exp(-x/tauT))/(1-T))+offset', 'independent', 'x', 'dependent', 'y','coefficients', {'n','offset','T','tauT'} );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [nstart offsetstart Tstart tauTstart];
opts.Lower = [nlow offsetlow Tlow tauTlow];
opts.Upper = [nhigh offsethigh Thigh tauThigh];
opts.Weights=weight;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
end

