%this function reads tif images and combines multi-page images to 1
function concimaged = read_tif2(imagestring)
%read the entire multi-image stack of tif
FileTif=imagestring;
InfoImage=imfinfo(FileTif);
mImage=InfoImage(1).Width;
nImage=InfoImage(1).Height;
NumberImages=length(InfoImage);
FinalImage=zeros(nImage,mImage,NumberImages,'uint16');
 
TifLink = Tiff(FileTif, 'r');
for i=1:NumberImages
   TifLink.setDirectory(i);
   FinalImage(:,:,i)=TifLink.read();
end
TifLink.close();

%combine the multi-images to 1 image
[m n k]=size(FinalImage);
if k==1
concimage=FinalImage(:,:,1);
else
    concimage=FinalImage(:,:,1);
for i=2:k
    concimage=[concimage;FinalImage(:,:,i)];
end
end 

%convert to double
concimaged=double(concimage);
end