before_selection=mean(clogbin_meanchunk,2);
before_selection_std=std(clogbin_meanchunk,1,2)/sqrt(size(clogbin_meanchunk,2));

%plot correlation curve
f3=figure( 'Name', 'averaged and logged' );
h3=plot( tlogbin, before_selection, 'b--.' );
hold on;
errorbar(tlogbin, before_selection, before_selection_std,'b','LineStyle','none');
legend( h3, 'data', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on

%plot with fit
[fitresultaverage, gofaverage] = createFit2D(tlogbin, before_selection,before_selection_std)
raverage=coeffvalues(fitresultaverage)

% ignore 0 standard deviation values just for plotting by including NaN
% values
% index=find(clogbin_stdchunk(:,i)==0);
% clogbin_stdchunk(index,i)=NaN;

% Plot fit with data.
f4=figure( 'Name', 'Fitted autocorrelation' );
h4 = plot( fitresultaverage, tlogbin, before_selection, 'b--.' );
hold on; 
errorbar(tlogbin, before_selection, before_selection_std,'b','LineStyle','none');
legend( h4, 'data', 'fit', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on