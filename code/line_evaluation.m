function [C]=line_evaluation(clogbin_lines,tlogbin,clogbin_lines_std)
prompt = {'Enter selector 0 to not fit all the lines per image chunk or 1 to fit all the lines of each image chunk'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d2=str2double(answer);

if d2==0
    str=["It was chosen to not fit all the autocorrelation lines"];
    C={str};
celldisp(C)
elseif d2==1
    for i=1:1:length(clogbin_lines)
        k=clogbin_lines(i);
        l=cell2mat(k);
        m=clogbin_lines_std(i);
        n=cell2mat(m);
        sz=size(l);
        r=[];
        for j=1:1:sz(2)
%fit: here choose which parameters to keep constant and which ones to alter
%according to the selector input

[fitresult, gof] = createFit2D(tlogbin, l(:,j),n(:,j) );
r(j,:)=coeffvalues(fitresult);

%ignore 0 standard deviation values just for the plotting by including NaN
%values
index=find(n(:,j)==0);
n(index,j)=NaN;

% Plot fit with data.
figure( 'Name', 'Fitted autocorrelation' );
h = plot( fitresult, tlogbin, l(:,j) );
hold on; 
errorbar(tlogbin, l(:,j), n(:,j),'b','LineStyle','none');
legend( h, 'data', 'fit', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
        end
        r_help{i}=r;
    end
    str=["gamma", "n", "D", "w_xy", "wz", "offset"];
C={str;r_help};
celldisp(C)
end
end