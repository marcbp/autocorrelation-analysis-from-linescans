%option 1 removes only the bleaching by cutting off the beginning up to
%tbleach
if bleaching==1
    
%removes the from the image selected amount of first lines
[tbleach,y1,button] = ginput;
%removes the beginning with the bleached values
Ir=Ir(tbleach:end,:);

%option 2 removes beginning up to tbleach and does movemean filtering
elseif bleaching==2
 
%removes the selected amount of lines and does a filtering with a movemean filter    
[tbleach,y1,button] = ginput;
%removes the beginning with the bleached values
Ir=Ir(tbleach:end,:);
    %choose filter parameters
prompt = {'Lower','Upper'};
dlgtitle = 'Enter filter parameter';
dims = [1 35];
definput = {'16384','16384'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
filterp=str2double(answer);
M = movmean(Ir,[filterp(1) filterp(2)],1); %if there are less points avaiable in the beginning or the end it takes the amount of points avaiable, see documentation
Irmean=mean(Ir);
Ir=Ir-M;
Ir=Ir+Irmean;

%option 3 removes the first specified value lines (here to leave 5*65536
%chunks for analysis)
elseif bleaching==3
    
%just cuts off the lines at the specified value
prompt = {'Initial cut off'};
dlgtitle = 'Enter filter parameter';
dims = [1 35];
definput = {'180225'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
tbleach=str2double(answer);
%removes the beginning with the bleached values
Ir=Ir(tbleach:end,:);

%option 4: cuts off the values up to a specified value and then does a
%movemean filtering
elseif bleaching==4
    
%cuts off the lines at the specified value and does a movemean filtering
%afterwards
prompt = {'Initial cut off'};
dlgtitle = 'Enter filter parameter';
dims = [1 35];
definput = {'180225'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
tbleach=str2double(answer);
%removes the beginning with the bleached values
Ir=Ir(tbleach:end,:);
    %choose filter parameters
prompt = {'Lower','Upper'};
dlgtitle = 'Enter filter parameter';
dims = [1 35];
definput = {'32768','32768'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
filterp=str2double(answer);
M = movmean(Ir,[filterp(1) filterp(2)],1); %if there are less points available in the beginning or the end it takes the amount of points avaiable, see documentation
Irmean=mean(Ir);
Ir=Ir-M;
Ir=Ir+Irmean;

%option 5: cuts off the values up to a specified value and then fits the averaged curve to a double exponential for correction 
elseif bleaching==5
    
%just cuts off the lines at the specified value
prompt = {'Initial cut off'};
dlgtitle = 'Enter filter parameter';
dims = [1 35];
definput = {'180225'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
tbleach=str2double(answer);
%removes the beginning with the bleached values
Ir=Ir(tbleach:end,:);

%creation of temporal vectors
tt=[0:1:size(Ir,1)-1]'*temp_res;
tt2=[0:1:size(imagevieweri,1)-1]'*temp_res*size(Ir,1)/1000;

%recalculation of bins depending on the cut-off size
for i=1:1000
    countercutf=floor(size(Ir,1)/1000);
    imageviewercutf(i,:)=mean(Ir((i-1)*countercutf+1:i*countercutf,:),1);
end

    %averages the data in 1000 chunks over time and then over all pixels
    %fits the result to a double exponential function and then subtracts
    %this function from the original data and adds the mean over time to
    %each pixel trace
    
Irs=mean(imageviewercutf,2);
irsstd=std(imageviewercutf,1,2);
Irm=mean(Ir,2);
Irmean=mean(Ir,1);
%prepares the data over time averaged data (1000) for the fit and fits it to the double exponential below
[xData, yData] = prepareCurveData( tt2, Irs );

% Set up fittype and options.
ft = fittype( 'afast*exp(-kfast*t)+aslow*exp(-kslow*t)+offset', 'independent', 't', 'dependent', 'y','coefficients', {'afast','kfast','aslow','kslow','offset'} );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [1 1 1 1 0];
%opts.StartPoint =[36 0.5 9 1];
opts.Lower = [-Inf -Inf -Inf -Inf -Inf];
opts.Upper = [Inf Inf Inf Inf Inf];
%opts.Weights=weight;

% Fit model to data.
[fitexp, gofexp] = fit( xData, yData, ft, opts );   

%calculation of the time trace by subtracting the fit from the original
%data
Irb=Ir-fitexp(tt);
Irbs=Irs-fitexp(tt2);
%asign new Ir
Ir=Irb+Irmean;

%option 6: cuts off the values up to a specified value and corrects each pixel individually by a double exponential
elseif bleaching==6
    
    %just cuts off the lines at the specified value
prompt = {'Initial cut off'};
dlgtitle = 'Enter filter parameter';
dims = [1 35];
definput = {'180225'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
tbleach=str2double(answer);
%removes the beginning with the bleached values
Ir=Ir(tbleach:end,:);

%creation of temporal vectors
tt=[0:1:size(Ir,1)-1]'*temp_res;
tt2=[0:1:size(imagevieweri,1)-1]'*temp_res*size(Ir,1)/1000;
Irmean=mean(Ir,1);

%recalculation of bins depending on the cut-off size
for i=1:1000
    countercutf=floor(size(Ir,1)/1000);
    imageviewercutf(i,:)=mean(Ir((i-1)*countercutf+1:i*countercutf,:),1);
end
    
    %averages the data in 1000 chunks over time and fits the result to a 
    %double exponential function for each pixel individually and then 
    %subtracts this function from the original data and adds the mean over 
    %time to each pixel trace

    %correction of individual fits and subtraction
for i=1:1:size(imageviewercutf,2)
     [xDatas, yDatas] = prepareCurveData( tt2, imageviewercutf(:,i) );

% Set up fittype and options.
fts = fittype( 'afast*exp(-kfast*t)+aslow*exp(-kslow*t)+offset', 'independent', 't', 'dependent', 'y','coefficients', {'afast','kfast','aslow','kslow','offset'} );
optss = fitoptions( 'Method', 'NonlinearLeastSquares' );
optss.Display = 'Off';
optss.StartPoint = [1 1 1 1 0];
%opts.StartPoint =[36 0.5 9 1];
%optss.Lower = [-Inf -Inf -Inf -Inf -Inf];
%optss.Upper = [Inf Inf Inf Inf Inf];
optss.Lower = [-5 -5 -5 -5 -5];
optss.Upper = [10 10 10 10 10];
%opts.Weights=weight;

% Fit model to data.
[fitexps, gofexps] = fit( xDatas, yDatas, fts, optss );

%save the fits
Irind(:,i)=fitexps(tt);
end
%calculate the difference and then add the mean of each curve
Irdif=Ir-Irind;
Irindmean=Irdif+Irmean;

%assign Ir
Ir=Irindmean;

%option 7: cuts off the values up to a specified value and then fits the averaged curve to a double exponential and uses a formula in Ries paper for correction
elseif bleaching==7
    
        %just cuts off the lines at the specified value
prompt = {'Initial cut off'};
dlgtitle = 'Enter filter parameter';
dims = [1 35];
definput = {'180225'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
tbleach=str2double(answer);
%removes the beginning with the bleached values
Ir=Ir(tbleach:end,:);

%creation of temporal vectors
tt=[0:1:size(Ir,1)-1]'*temp_res;
tt2=[0:1:size(imagevieweri,1)-1]'*temp_res*size(Ir,1)/1000;

%recalculation of bins depending on the cut-off size
for i=1:1000
    countercutf=floor(size(Ir,1)/1000);
    imageviewercutf(i,:)=mean(Ir((i-1)*countercutf+1:i*countercutf,:),1);
end

   %fit of double exponential to indvidual lines
for i=1:1:size(imageviewercutf,2)
     [xData, yData] = prepareCurveData( tt2, imageviewercutf(:,i) );

% Set up fittype and options.
fts = fittype( 'a1*exp(-k1*t)+a2*exp(-k2*t)+a0', 'independent', 't', 'dependent', 'y','coefficients', {'a1','k1','a2','k2','a0'} );
optss = fitoptions( 'Method', 'NonlinearLeastSquares' );
optss.Display = 'Off';
optss.StartPoint = [1 1 1 1 0];
%opts.StartPoint =[36 0.5 9 1];
% optss.Lower = [-Inf -Inf -Inf -Inf -Inf];
% optss.Upper = [Inf Inf Inf Inf Inf];
optss.Lower = [-1 -1 -1 -1 -10];
optss.Upper = [10 10 10 10 10];
%opts.Weights=weight;

% Fit model to data.
[fitexps, gofexps] = fit( xData, yData, fts, optss );

%save the fits
Irind(:,i)=fitexps(tt);
end

 %correction from the Ries paper
intenscorr=Ir./sqrt(Irind./Irind(1,:))+Irind(1,:).*(1-sqrt(Irind./Irind(1,:)));

Ir=intenscorr;

end

%recalculation of bins depending on the cut-off size
for i=1:1000
    counter2=floor(size(Ir,1)/1000);
    imageviewer2(i,:)=mean(Ir((i-1)*counter2+1:i*counter2,:),1);
end

f12=figure('Name','Averaged line profile')
%imagesc(imageviewer);
plot([0:1:size(imageviewer2)-1]*size(Ir,1)/1000,mean(imageviewer2,2));
%colorbar
%set(gca, 'YScale', 'log')
title('Temorally averaged intensity profile (1000 bins)')
ylabel('Average intensity [a.u.]')
xlabel('bins/lines')
