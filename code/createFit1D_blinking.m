function [fitresult, gof] = createFit1D_blinking(t, c,std)

%presettings
prompt = {'Enter gamma:','Enter n:','Enter D:','Enter w_{xy}:','Enter offset','Enter selector:','Enter fraction of triplet state:','Enter triplet state constant:'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0.35','50','0.1','0.33','0.001','2','.2','0.1'};
answer = inputdlg(prompt,dlgtitle,dims,definput)
d=str2double(answer)

gamma=d(1);
n=d(2);
D=d(3);
wxy=d(4);
offset=d(5);
fix=d(6);
T=d(7);
tauT=d(8);

%create weights out of standard deviation by weighing the ones with a low
%standard deviation more than the other way around;replace the Inf values
%with 0 to give the first values low importance
weight=1./std;
index=find(weight==Inf);
weight(index)=0;

if fix==0
    gammastart=0.35;
    gammalow=0;
    gammahigh=+Inf;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0.000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
elseif fix==1
        gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==2
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=wxy;
    wxylow=wxy;
    wxyhigh=wxy;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==3
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=wxy;
    wxylow=wxy;
    wxyhigh=wxy;
    offsetstart=offset;
    offsetlow=offset;
    offsethigh=offset;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==4
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=D;
    Dlow=D;
    Dhigh=D;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
    elseif fix==5
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=n;
    nlow=n;
    nhigh=n;
    Dstart=D;
    Dlow=D;
    Dhigh=D;
    wxystart=0.25;
    wxylow=0;
    wxyhigh=+Inf;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=0;
    tauThigh=100;
      elseif fix==6
             gammastart=gamma;
    gammalow=gamma;
    gammahigh=gamma;
    nstart=50;
    nlow=0;
    nhigh=+Inf;
    Dstart=0.15;
    Dlow=0;
    Dhigh=+Inf;
    wxystart=wxy;
    wxylow=wxy;
    wxyhigh=wxy;
    offsetstart=0.0000001;
    offsetlow=-5;
    offsethigh=5;
    Tstart=T;
    Tlow=0;
    Thigh=1;
    tauTstart=tauT;
    tauTlow=tauT;
    tauThigh=tauT;
end

%% Fit: 
%only prepares the vectors to be column vectors
%here transpose the vectors only
[xData, yData] = prepareCurveData( t, c );

% Set up fittype and options.
ft = fittype( '(gamma/n)*(1./sqrt(1+4*D*x/(wxy^2))).*((1-T+T*exp(-x/tauT))/(1-T))+offset', 'independent', 'x', 'dependent', 'y','coefficients', {'gamma','n','D','wxy','offset','T','tauT'} );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [gammastart nstart Dstart wxystart offsetstart Tstart tauTstart];
opts.Lower = [gammalow nlow Dlow wxylow offsetlow Tlow tauTlow];
opts.Upper = [gammahigh nhigh Dhigh wxyhigh offsethigh Thigh tauThigh];
opts.Weights=weight;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
end

