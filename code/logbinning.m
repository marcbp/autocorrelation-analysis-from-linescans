function [tavg,cavg,cstd]=logbinning(t,c,bins)
t=t(2:end);
c=c(2:end);
n=length(c);
index=zeros([bins 2]);
tavg=zeros([bins 1]);
cavg=zeros([bins 1]);
cstd=zeros([bins 1]);
for i=1:1:bins
   index(i,:)=[floor(exp((i-1)*log(n)/bins)), floor(exp((i)*log(n)/bins))];
   tavg(i)=mean(t(index(i,1):index(i,2)));
   cavg(i)=mean(c(index(i,1):index(i,2)));
   cstd(i)=std(c(index(i,1):index(i,2)),1);
end
end