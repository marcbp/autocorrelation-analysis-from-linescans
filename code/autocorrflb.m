%autocorrfl floors d if it is not of the right size and ignores the data
%which are too much at the end; it also doesn't initialize g; autocorr does
%both of the things
function [t,c,g,m]=autocorrflb(data,filter,time)
%check if value is of type double and convert it if necessary
if isa(data,'double')==1
    data=data;
else
    data=double(data);
end

%%
%define the amount of pieces in which the column is cut = filtering
v=filter;
d=floor(length(data)/v);
%create/initialize a vector which is of the desired length with 2*d-1

%doesn't need the initialization
%g=zeros([v 2*d-1]); %pad at least with 2*length-1, bc that's what the 
%convoluted signal needs to be linear
%g=zeros([v 4*d]); %only pad with 2*length-1 bc everything above is too
%high and everything below too short
if isempty(time)
ts=0.005;
else ts=time;
end

%%
%for each piece of the vector calculate the correlation via fft with
%zero-padding
for i=1:1:v
    %first part is to cut the vector into pieces v
    b(i,1:d)=data((i-1)*d+1:i*d);
end
    %calculate the mean of each piece
    me=mean(b,2);
    %calculate the fft of the vector and pad to the desired length for
    %linear fft
    %calculate the difference from the mean
    b2=b-me;
B=fft(b2,2*d-1,2);
%calcualate the power spectral density
G=B.*conj(B);%it doesn't matter if divided by the length (d) here or later 
g=ifft(G,2*d-1,2); %since the vector g has a length of 2n-1 we have to 
%devide by 2n-1 instead of n (definition of fft in matlab)
g=g./((2*d-1)*me.^2);


t=(0:1:d-1)*ts;
%eliminates NaN values
[m, ~]=find(isnan(g));
g(m,:)=[];
if filter==1
    c=g;
else
    c=mean(g,1);
end
c=c(1:d);
g=g(:,1:d);
end