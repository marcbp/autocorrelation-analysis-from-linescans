function [Clogbinm,CStd,Clog_orig,Clog_final] = ac_selectorp_axis(tlogbin, Clogbin)
Clog_final=[];
for i=1:1:size(Clogbin,2)
    Clog_orig(:,:,i)=Clogbin(:,i,:);
    Clog_help=[];
    Clog_help (:,:)=Clog_orig(:,:,i);
    
    %plot the AC before the selection of lines
faca=figure('Name','ACs adjusted')
plot( tlogbin, mean(Clog_help,2), 'b--.' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
%moves the position on the screen
movegui(faca,'east');
%plots the image as a colormap with each autocorrelation going from the top
%to the bottom (over time) and each chunks along the x-axis
fima=figure('Name','Color-coded ACs')
%assigns correct temporal axis labels
%need to calculate the ticks and labels in a function otherwise it messes
%up counter i
[ticks,tlabels]=assign_correct_labels(tlogbin, Clog_help);
imagesc(Clog_help);
yticks(ticks);
yticklabels(tlabels);
colorbar
%set(gca, 'YScale', 'log')
title('Color-coded ACs')
ylabel('Time lag \tau [s]')
xlabel('AC [a.u.]')
%impixelinfo
%moves the position on the screen
movegui(fima,'west');
%have the selection if there are any bad autocorrelation chunks
prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d=str2double(answer);

%here only remove the colums from the logbinned vectors and ask if there
%are more to erase after a first selection; then cut the matrix and take
%only the good ones for further calculations
while d==1
   figure(fima);
   [px,yp,Pp] = impixel;
    Clog_help(:,px)=[];
%corrects the time axis of the imagesc
imagesc(Clog_help)
yticks(ticks);
yticklabels(tlabels);
colorbar
title('Color-coded ACs')
ylabel('Time lag \tau [s]')
xlabel('AC [a.u.]')
%impixelinfo
%replot the AC
figure(faca);
clf
plot( tlogbin, mean(Clog_help,2), 'b--.' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
dlgtitle = 'Input';
%dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d=str2double(answer);
end
%calculation of the mean over the pixels for each chunk
Clog_final=[Clog_final Clog_help];
end
%calculation of the mean over the last 2 dimensions
Clogbinm=mean(Clog_final,2);
CStd=std(Clog_final,1,2)/sqrt(size(Clog_final,2)); %here 0 means that the division is done with n-1 instead of n
end