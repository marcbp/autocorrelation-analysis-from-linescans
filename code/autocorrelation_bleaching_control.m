clear all
close all

%%
%string to image
string='Filepath\filename.tif';

%reads in tif images properly and combines tif stacks
Ir = read_tif2(string);
%%
%determines dimensions of image
sz = size(Ir)

%define parameters such as line scan frequency, temporal chunk length and
%binning of time steps
prompt = {'Enter temporal resolution:','Enter length of pieces to analyze:','Enter bin number:'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'1800','65536','64'};
answer = inputdlg(prompt,dlgtitle,dims,definput)
parameters=str2double(answer)

temp_res=1/parameters(1);
length_chunks=parameters(2);
bins=parameters(3);

%for display purposes averages all time points into equal 500 points
for i=1:500
    counter=floor(sz(1)/500);
    imageviewer(i,:)=mean(Ir((i-1)*counter+1:i*counter,:),1);
end

%plot the image over the averaged parts
f22=figure('Name','Averaged image chunks')
%imagesc(imageviewer);
imagesc([0:1:sz(2)],[0:counter:sz(1)]*temp_res,imageviewer);
%colorbar
%set(gca, 'YScale', 'log')
title('Line Scan with averaged temporal values (500)')
ylabel('Time [s]')
xlabel('Pixel')
xticks([0:10:sz(2)])

%%
%let's one select the points in an image and returns the coordinates of the
%points in x and y as column and row vectors respectively
%2021-06-01 adds if clause in case there is only 1 pixel, impixel requires
%mroe than one pixel to work
if size(Ir,2)==1
    x=1;
else
[x,y,P] = impixel
end
%%
%plots the averaged intensity over time to see bleaching
f11=figure('Name','Mean intensity')
plot((1:1:sz(1)),mean(Ir,2))
ylabel('Average intensity [a.u.]')
xlabel('bins')

%plot the averaged intensity profile, here 1000 bins with 303,104/507,904 lines
%each (also to get a better overview)
for i=1:1000
    counteri=floor(sz(1)/1000);
    imagevieweri(i,:)=mean(Ir((i-1)*counteri+1:i*counteri,:),1);
end

f1=figure('Name','Averaged line profile')
plot([0:1:size(imagevieweri,1)-1]*size(Ir,1)/1000,mean(imagevieweri,2));
title('Temorally averaged intensity profile (1000 bins)')
ylabel('Average intensity [a.u.]')
xlabel('bins/lines')


%remove the initial times points with bleaching
prompt = {'Options: 0 - no filtering, no cut off, 1- cut off, 2 - cut off and filtering, 3 - specify cut off value, 4 - specify cut off value and filtering'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
bleaching=str2double(answer);

%initialize to not remove any time points
tbleach=0;

%let's one choose how to treat bleaching
bleaching_treatment

%redetermines size (for the case of removal of time points)
sz2 = size(Ir);
%and calculate the amount of chunks new
amount_chunks=sz2(1)/length_chunks;
%devide the time into averaged pieces to better get an idea of the tubules
%over the entire time - replot adjusted
for i=1:500
    counter=floor(sz2(1)/500);
    imageviewer(i,:)=mean(Ir((i-1)*counter+1:i*counter,:),1);
end

%plot the image over the averaged parts
f2=figure('Name','Averaged image chunks')
imagesc([0:1:sz(2)],[tbleach:counter:sz(1)]*temp_res,imageviewer);
title('Line Scan with averaged temporal values (500)')
ylabel('Time [s]')
xlabel('Pixel')

%%
%selector whether it should analyze 1 line, option 0, more lines, option 1,
%or ranges, option 2
%mainly option 2 since it allows for a selection of multiple groups of lines together
prompt = {'Enter selector 0 to analyze 1 line, enter 1 to analyze more lines and enter 2 to analyze ranges'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d=str2double(answer);

%%
% initialize the counter and cell to store the image pieces and start the
% if condition to analyze chunks (more than 1 line) in a region of the
% image
j=0;
%choose option 2 to select multiple concatenated columns and even more than
%1 of these blocks of lines (if condition to analyze chunks (more than 1 line) in a region of the
% image)
if d==2
% initialize the cell to store the image pieces
imgCell=cell(1, length(x)-1);

%selection of image pieces until lenth-1
for i=1:length(x)-1
   %cut the image into pieces as defined; uses 2 adjacent points to
   %calculate the piece of the image
Ic=imcrop(Ir,[x(i) 1 x(i+1)-x(i) sz(1)-1]);%[xmin ymin width height]
%store the image pieces into cells
imgCell{i}=Ic;
% optionally: display the image pieces
%  figure(i+2)
% imshow(Ic)

%%
%select the pieces which are desired with odd numbers in the vector because
%even numbers are the not desired pieces since regions between odd and even 
%numbers are desired; even and odd are just space between the desired
%regions; example: region between x= 1 and 2 is supposed to be analyzed,
%then with the next 2 points 3 and 4 another region is selected, but the
%image crop also crops from point 2-3 as region 2 (this is the undesired 
%space). So this checks the beginnings of the chunks and only if the
%remainder is 1 by deviding by 2 the image piece is the desired selection
%This is selected here and stored into Iss
 if mod(i,2)==1
        j=j+1;
        Iss{j}=imgCell{i};
    end
end

%%
% optional: shows the image selections which are desired and cut out
% for i=1:length(Iss)
%     figure(i+10)
%     imshow(cell2mat(Iss(i)))
% end

%%
%select the piece and calculate the autocorrelation
for i=1:length(Iss)
    k=cell2mat(Iss(i));
%converts the image data from uint8 to double since with uint8 it doesn't
%go below 0 -> wrong results
    l=double(k);

%initialize values so they have the correct size
t=[];
g=[];
c=[];
tlogbin=[];
clogbin=[];
clogstd=[];
clogbin_help=[];
clogbin_std=[];
clogbinm=[];
clogbinstd=[];
Clogbin=[];
G=[];
Clogstd=[];
valuenan=[];

    for j=1:length(l(1,:))
   %do not initialize the values inside the loop. This overwrites the
        %previous values each time and for the mean value the sum is still
        %divided by the number of pixels, but all the pixels except the
        %last are 0, therefore the amplitude is smaller by the size of the
        %pixels
        %g=[];
        %c=[];
        %clogbin=[];
        %clogbin_help=[];
        %clogbinm=[];
        %clogstd=[];
        
        %optional code with fft or loop (use fft since faster)
        [t,c(j,:),g(:,:),valuenan{j}]=autocorrflb(l(:,j),amount_chunks,temp_res);
        %calculates the logbinned chunks depending on the binsize:bins
        %either logbinmatrix_remove_rep_full which removes replicates, if replicates at
        % the beginning of the vector are fine use instead logbinmatrix
        [tlogbin,clogbin(:,:),clogstd(:,:)]=logbinmatrix_remove_rep_full(t,g(:,:),bins);
        %calculates the dimensions of all chunks of logbinned autocorrelations
        
        G(:,:,j)=g;
        Clogbin(:,:,j)=clogbin;
        Clogstd(:,:,j)=clogstd;
        
[m, n]=size(clogbin(:,:));


% %plots the image as a colormap with each autocorrelation going from the top
% %to the bottom (over time) and each chunks along the x-axis
% figure('Name','Color-coded ACs')
% Selector=imagesc([1 n],[tlogbin(1) tlogbin(end)],clogbin(:,:));
% colorbar
% %set(gca, 'YScale', 'log')
% title('Color-coded ACs')
% ylabel('Time lag \tau [s]')
% xlabel('AC [a.u.]')
% 
% %have the selection if there are any bad autocorrelation chunks
% prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
% dlgtitle = 'Input';
% dims = [1 35];
% definput = {'0'};
% answer = inputdlg(prompt,dlgtitle,dims,definput);
% d=str2double(answer);

%assign help clogbin because in a 3D matrix the dimensions have to be the
%same, therefore store the matrix in a seperate variable and calculate the
%mean before moving on
%clogbin_help holds all the autocorrelations of all the cuts of 1 line
clogbin_help=clogbin(:,:);
clogbin_std=clogstd(:,:);

%here only remove the colums from the logbinned vectors and ask if there
%are more to erase after a first selection; then cut the matrix and take
%only the good ones for further calculations
% while d==1
%     [px,yp,Pp] = impixel;
%     clogbin_help(:,px)=[];
%     clogbin_std(:,px)=[];
% imagesc(clogbin_help)
% colorbar
% prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
% dlgtitle = 'Input';
% %dims = [1 35];
% definput = {'0'};
% answer = inputdlg(prompt,dlgtitle,dims,definput);
% d=str2double(answer);
% end

%now calculate the mean of all the logbinned autocorrelations
%here it is the same if first averaging and then logbinning or first
%logbinning of each chunk and then averaging over all af the chunks; not
%identical, but very similar
%clogbinm holds all the mean autocorrelations of all the lines in 1 chunk
%of image
clogbinm(:,j)=mean(clogbin_help,2);
%calculates the standard deviation of 1 line over all the segments
clogbinstd(:,j)=std(clogbin_help,1,2)/sqrt(size(clogbin_help,2));
    end
 
 %first averaging and logbinning
 cmean(i,:)=mean(c,1);
[tlogbin2,a(i,:),b(i,:)]=logbinning(t,cmean(i,:),bins);  
% Plot fit with data.
f3=figure( 'Name', 'averaged and logged' );
h2=plot( tlogbin2, a(i,:), 'b--.' );
% hold on; 
% errorbar(tlogbin, clogbin_meanchunk(:,i), clogbin_stdchunk(:,i),'b','LineStyle','none');
legend( h2, 'data', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on

    %clogbin_meanchunk holds the averaged autocorrelations of all lines
    %averaged together for each image chunk seperately
    clogbin_meanchunk(:,i)=mean(clogbinm,2);
    %this holds all the standard deviations averaged together from the
    %logbinning; 
    %this holds the standard deviation of here the standard error of the
    %averaged chunks over each line in a chunk
    clogbin_stdchunk(:,i)=std(clogbinm,1,2)/sqrt(size(clogbinm,2));
    
    %clogbin_lines holds all the autocorrelations of each line in 1 chunk
    %on a seperate page; it has to be a cell array because a 3D matrix has
    %to have the same dimensions for the matrices inside the 3rd dimension
    clogbin_lines{i}=clogbinm;
    %holds the standard deviations of each line for a chunk on 1 page
    clogbin_lines_std{i}=clogbinstd;
    %saves in a cell array of each image chunk all the ACs in chunks of
    %each pixel
    ACs_raw{i}=G;
    ACs{i}=Clogbin;

%fit: here choose which parameters to keep constant and which ones to alter
%according to the selector input
%choose fitting model and output of values

[fitresult, gof] = createFit1D(tlogbin, clogbin_meanchunk(:,i),clogbin_stdchunk(:,i))
r(i,:)=coeffvalues(fitresult)

%ignore 0 standard deviation values just for plotting by including NaN
%values
index=find(clogbin_stdchunk(:,i)==0);
clogbin_stdchunk(index,i)=NaN;

% Plot fit with data.
f4=figure( 'Name', 'Fitted autocorrelation' );
h = plot( fitresult, tlogbin, clogbin_meanchunk(:,i), 'b--.' );
hold on; 
errorbar(tlogbin, clogbin_meanchunk(:,i), clogbin_stdchunk(:,i),'b','LineStyle','none');
% hold on
% plot(tlogbin2, a(i,:), 'g--.')
legend( h, 'data', 'fit', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
end

%produce the output labels in str and combine them in Cmean, display them
%afterwards
str=["gamma", "n", "D", "w_xy", "wz", "offset"];
Cmean={str;r};
celldisp(Cmean)

%select wheter to fit all the lines 1 by 1 and to get a 1 by 1 output or to
%get just the data of the averaged autocorrelation
%[C]=line_evaluation(clogbin_lines,tlogbin,clogbin_lines_std);


%the condition for d=0 only works for the selection of 1 line
elseif d==0
    l=double(Ir(:,x));
        %[t,c,g]=autocorr(l,length(l)/1024,5e-6);
        %[fitresult]=autocorrelator(l,temp_res,length_chunks,bins);
        
%plots the amplitudes
f1=figure('Name','Amplitude')
plot(temp_res*(1:1:length(l)),l)
%title('Raw amplitudes over counts')
xlabel('Time t [s]')
ylabel('Amplitudes [a.u.]')

%calculate the autocorrelation
[t,c,g, valuenan]=autocorrflb(l,amount_chunks,temp_res);

%calculates the logbinned chunks depending on the binsize:bins
[tlogbin,clogbin,clogstd]=logbinmatrix_remove_rep(t,g,bins);
%calculated the dimensions of all chunks of logbinned autocorrelations
[m, n]=size(clogbin);
%plot the AC before the selection of lines (2021-06-01)
faca=figure('Name','ACs cleaned')
plot( tlogbin, mean(clogbin,2), 'b--.' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
%moves the position on the screen
movegui(faca,'east');
%plots the image as a colormap with each autocorrelation going from the top
%to the bottom (over time) and each chunks along the x-axis; time vs.
%temporal chunk
f2=figure('Name','Color-coded ACs')
%assigns correct temporal axis labels
%need to calculate the ticks and labels in a function otherwise it messes
%up counter i
[ticks,tlabels]=assign_correct_labels(tlogbin, clogbin);
imagesc(clogbin(:,:));
yticks(ticks);
yticklabels(tlabels);
colorbar
title('Color-coded ACs')
ylabel('Time lag \tau [s]')
xlabel('AC chunks [a.u.]')

%moves the position on the screen
movegui(f2,'west');

%have the selection if there are any bad autocorrelation chunks
prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d=str2double(answer);

%here only remove the colums from the logbinned vectors and ask if there
%are more to erase after a first selection; then cut the matrix and take
%only the good ones for further calculations
while d==1
    figure(f2);
    [xp,yp,Pp] = impixel;
    clogbin(:,xp)=[];
    clogstd(:,xp)=[];
    imagesc(clogbin)
    %use the counter from above
yticks(ticks);
yticklabels(tlabels);
colorbar
title('Color-coded ACs')
ylabel('Time lag \tau [s]')
xlabel('AC chunks [a.u.]')
%replot the AC with selected ACs (2021-06-01)
figure(faca);
clf
plot( tlogbin, mean(clogbin,2), 'b--.' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
dlgtitle = 'Input';
%dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d=str2double(answer);
end

%now calculate the mean of all the logbinned autocorrelations
%here it is the same if first averaging and then logbinning or first
%logbinning of each chunk and then averaging over all af the chunks; not
%identical, but very similar
clogbinm=mean(clogbin,2);
%clogbinstd=mean(clogstd,2);
clogbinstd=std(clogbin,1,2)/sqrt(size(clogbin,2));

%fit: here choose which parameters to keep constant and which ones to alter
%according to the selector input

[fitresult, gof] = createFit2D(tlogbin, clogbinm,clogbinstd )
r=coeffvalues(fitresult)

%ignore 0 standard deviation values just for the plotting by including NaN
%values
index=find(clogbinstd==0);
clogbinstd(index)=NaN;

% Plot fit with data.
f4=figure( 'Name', 'Fitted autocorrelation' );
h = plot( fitresult, tlogbin, clogbinm, 'b--.' );
hold on; 
errorbar(tlogbin, clogbinm, clogbinstd,'b','LineStyle','none');
legend( h, 'data', 'fit', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
       
% if d=1 then if takes only the selected lines and calculates and fits the
% autocorrelation for these lines
elseif d==1
    
    %store the selected lines in a vector l
    l=double(Ir(:,x));
    
 %initialize values so they have the correct size   
g=[];
c=[];
clogbin=[];
clogbin_help=[];
clogbinm=[];
clogstd=[];
clogbinstd=[];
    
    %now calculate the autocorrelations and logbin them
    for i=1:1:length(x)
%                 g=[];
% c=[];
% clogbin=[];
% clogbin_help=[];
% clogbinm=[];
% clogstd=[];
% clogbinstd=[];
        
        [t,c(i,:),g(:,:), valuenan{i}]=autocorrflb(l(:,i),amount_chunks,temp_res);
        %calculates the logbinned chunks depending on the binsize:bins
        [tlogbin,clogbin(:,:),clogstd(:,:)]=logbinmatrix_remove_rep(t,g(:,:),bins);
        
        %save all the data
        G(:,:,i)=g;
        Clogbin(:,:,i)=clogbin;
        Clogstd(:,:,i)=clogstd;
%calculated the dimensions of all chunks of logbinned autocorrelations
[m, n]=size(clogbin(:,:));

%plot the AC before the selection of lines (2021-06-01)
faca=figure('Name','ACs cleaned')
plot( tlogbin, mean(clogbin,2), 'b--.' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
%moves the position on the screen
movegui(faca,'east');

%plots the image as a colormap with each autocorrelation going from the top
%to the bottom (over time) and each chunks along the x-axis
f2=figure('Name','Color-coded ACs')
%need to calculate the ticks and labels in a function otherwise it messes
%up counter i
[ticks,tlabels]=assign_correct_labels(tlogbin, clogbin);
imagesc(clogbin(:,:));
yticks(ticks);
yticklabels(tlabels);
colorbar
%set(gca, 'YScale', 'log')
title('Color-coded ACs')
ylabel('Time lag \tau [s]')
xlabel('AC chunks [a.u.]')

%moves the position on the screen
movegui(f2,'west');

%have the selection if there are any bad autocorrelation chunks
prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
dlgtitle = 'Input';
dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d=str2double(answer);

%assign help clogbin because in a 3D matrix the dimensions have to be the
%same, therefore store the matrix in a seperate variable and calculate the
%mean before moving on
clogbin_help=clogbin(:,:);
clogbin_std=clogstd(:,:);

%here only remove the colums from the logbinned vectors and ask if there
%are more to erase after a first selection; then cut the matrix and take
%onyl the good ones for further calculations
while d==1
    figure(f2);
    [xr,yr,Pr] = impixel;
    clogbin_help(:,xr)=[];
    clogbin_std(:,xr)=[];
imagesc(clogbin_help)
%use the counter from above
yticks(ticks);
yticklabels(tlabels);
colorbar
title('Color-coded ACs')
ylabel('Time lag \tau [s]')
xlabel('AC chunks [a.u.]')
%replot the AC with selected ACs (2021-06-01)
figure(faca);
clf
plot( tlogbin, mean(clogbin_help,2), 'b--.' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
prompt = {'Enter selector 0 to not remove any lines or 1 to remove lines'};
dlgtitle = 'Input';
%dims = [1 35];
definput = {'0'};
answer = inputdlg(prompt,dlgtitle,dims,definput);
d=str2double(answer);
end

%now calculate the mean of all the logbinned autocorrelations
%here it is the same if first averaging and then logbinning or first
%logbinning of each chunk and then averaging over all af the chunks; not
%identical, but very similar
clogbinm(:,i)=mean(clogbin_help,2);

clogbinstd(:,i)=std(clogbin_help,1,2)/sqrt(size(clogbin_help,2));

%fit: here choose which parameters to keep constant and which ones to alter
%according to the selector input

[fitresult, gof] = createFit2D(tlogbin, clogbinm(:,i),clogbinstd(:,i) )
r(i,:)=coeffvalues(fitresult)

%ignore 0 standard deviation values just for the plotting by including NaN
%values
index=find(clogbinstd(:,i)==0);
clogbinstd(index,i)=NaN;

% Plot fit with data.
f4=figure( 'Name', 'Fitted autocorrelation' );
h = plot( fitresult, tlogbin, clogbinm(:,i),'b--.');
hold on; 
errorbar(tlogbin, clogbinm(:,i), clogbinstd(:,i),'b','LineStyle','none');
legend( h, 'data', 'fit', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
    end
    
%produce the string and get the output, display the fitted data in a matrix
str=["gamma", "n", "D", "w_xy", "wz", "offset"];
C={str;r};
celldisp(C)
end