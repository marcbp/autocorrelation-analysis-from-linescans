%determine the amplitude at 0 at the fit and normalize the entire data to
%it

%calculation of normalization values
clognorm=Clogbinm/fitresultav(0);

%normalization of error (through error propagation)
stdnorm=CStd/fitresultav(0);

% Plot fit with data.
fnorm=figure( 'Name', 'normalized autocorrelation' );
plot(  tlogbin, clognorm, 'b--.' );
hold on; 
plot( tlogbin,fitresultav(tlogbin)/fitresultav(0) );
errorbar(tlogbin, clognorm, stdnorm,'b','LineStyle','none');
legend( 'data', 'fit', 'Location', 'NorthEast' );
set(gca, 'XScale', 'log')
% Label axes
xlabel 'Time lag \tau [s]'
ylabel 'AC [a.u.]'
grid on
