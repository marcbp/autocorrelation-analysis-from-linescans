function [tlogbin,clogbin,clogstd]=logbinmatrix(t,g,bins)

[m, ~]=size(g);

clogbin=zeros([bins m]);
clogstd=zeros([bins m]);

for i=1:1:m
   
   [tlogbin,a,b]=logbinning(t,g(i,:),bins); 
   clogbin(:,i)=a;
   clogstd(:,i)=b;
   
end
end